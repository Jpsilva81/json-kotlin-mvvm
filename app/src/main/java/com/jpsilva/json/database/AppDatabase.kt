package com.jpsilva.json.database

import android.arch.persistence.room.Database
import android.arch.persistence.room.RoomDatabase
import com.jpsilva.json.model.Post
import com.jpsilva.json.model.PostDao

@Database(entities = arrayOf(Post::class), version = 1)
abstract class AppDatabase : RoomDatabase() {
    abstract fun postDao(): PostDao
}