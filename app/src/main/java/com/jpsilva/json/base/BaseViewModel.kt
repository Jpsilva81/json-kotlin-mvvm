package com.jpsilva.json.base

import android.arch.lifecycle.ViewModel
import com.jpsilva.json.injection.component.DaggerViewModelInjector
import com.jpsilva.json.injection.component.ViewModelInjector
import com.jpsilva.json.module.NetworkModule
import com.jpsilva.json.ui.post.PostListViewModel

abstract class BaseViewModel:ViewModel(){
    private val injector: ViewModelInjector = DaggerViewModelInjector
            .builder()
            .networkModule(NetworkModule)
            .build()

    init {
        inject()
    }

    /**
     * Injects the required dependencies
     */
    private fun inject() {
        when (this) {
            is PostListViewModel -> injector.inject(this)
        }
    }
}